<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function index()
    {
        $casts = DB::table('casts')->get();
        return view('casts.index', compact('casts'))->with('title', 'All Cast');
    }
    public function create()
    {
        return view('casts.create', ['title' => 'Tambah Artis Baru']);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'age' => 'required|numeric',
            'bio' => 'required'
        ]);
        $query = DB::table('casts')->insert([
            'name' => $request["name"],
            'age' => $request["age"],
            'bio' => $request["bio"]
        ]);
        return redirect('/cast')->with('success', 'Berhasil Dismpan');
    }
    public function show($id)
    {
        $cast = DB::table('casts')->where('id', $id)->first();
        return view('casts.show', compact('cast'))->with('title', 'Profil');
    }
    public function edit($id)
    {
        $cast = DB::table('casts')->where('id', $id)->first();
        // dd($cast);
        return view('casts.edit', compact('cast'))->with('title', 'Edit Profil');
        return redirect('/cast');
    }
    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required',
            'age' => 'required|numeric',
            'bio' => 'required'
        ]);

        $query = DB::table('casts')
            ->where('id', $id)
            ->update([
                'name' => $request["name"],
                'age' => $request["age"],
                'bio' => $request["bio"]
            ]);
        return redirect('/cast');
    }

    public function destroy($id)
    {
        $query = DB::table('casts')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
