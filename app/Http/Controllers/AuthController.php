<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index()
    {
        return view('register');
    }
    public function welcome(Request $request)
    {
        return view('welcome_after_signin', ['request' => $request]);
    }
}
