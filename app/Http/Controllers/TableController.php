<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TableController extends Controller
{
    public function index()
    {
        return view('tugas3.table', ['title' => 'Table']);
    }
    public function data_tables()
    {
        return view('tugas3.data-tables', ['title' => 'DataTables']);
    }
}
