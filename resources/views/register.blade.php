<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Sign Up</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="register" method="post">
        @csrf
      <label for="nama_awal">First Name :</label><br /><br />
      <input type="text" name="nama_awal" id="nama_awal" /><br /><br />

      <label for="nama_akhir">Last Name :</label><br /><br />
      <input type="text" name="nama_akhir" id="nama_akhir" /><br /><br />

      <label>Gender :</label><br /><br />
      <input type="radio" name="kelamin" id="pria" value="pria" />
      <label for="pria">Male</label><br />
      <input type="radio" name="kelamin" id="wanita" value="wanita" />
      <label for="wanita">Female</label><br />
      <input type="radio" name="kelamin" id="other-sex" value="other" />
      <label for="other-sex">Other</label><br /><br />

      <label for="nationality">Nationality :</label><br /><br />
      <select name="nationality" id="nationality">
        <option value="indonesia">Indonesian</option>
        <option value="malayasia">Malayasian</option>
        <option value="australia">Australian</option>
      </select>
      <br /><br />

      <label>Language Spoken :</label><br /><br />
      <input type="checkbox" id="bahasa" name="lang" value="bahasa" />
      <label for="bahasa">Bahasa Indonesia</label><br />
      <input type="checkbox" id="english" name="lang" value="english" />
      <label for="english">English</label><br />
      <input type="checkbox" id="other-lang" name="lang" value="other" />
      <label for="other-lang">Other</label><br /><br />

      <label for="bio">Bio :</label><br /><br />
      <textarea name="bio" id="bio" cols="40" rows="6"></textarea><br /><br />

      <input type="submit" value="Sign Up" />
    </form>
  </body>
</html>
