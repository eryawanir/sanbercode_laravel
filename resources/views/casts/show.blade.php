@extends('layouts.master')

<!-- Content Header (Page header) -->
@section('header')
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <h1 class="text-center">{{$title}}</h1>
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">{{$title}}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
@endsection

<!-- Main content -->
@section('content')
<div class="row d-flex justify-content-center">
<div class="col-7">
    <div class="card card-primary card-outline">
        <div class="card-body box-profile">
          <div class="text-center">
            <img class="profile-user-img img-fluid img-circle" src="../../dist/img/user4-128x128.jpg" alt="User profile picture">
          </div>

          <h3 class="profile-username text-center">{{$cast->name}}</h3>

          <p class="text-muted text-center">{{$cast->age}} tahun</p>

          <ul class="list-group list-group-unbordered mb-3">
            <li class="list-group-item text-center"> {{$cast->bio}}
            </li>
          </ul>

          <a href="/cast" class="btn btn-primary btn-block"><b>Kembali</b></a>
          <a href="/cast/{{$cast->id}}/edit" class="btn btn-success btn-block"><b>Edit</b></a>
          <form action="/cast/{{$cast->id}}" method="POST" class="my-2">
            @csrf
            @method('DELETE')
            <input type="submit" class="btn btn-danger btn-block" value="Delete">
           </form>
        </div>
        <!-- /.card-body -->
    </div>

</div>
</div>
@endsection
    <!-- /.content -->
    