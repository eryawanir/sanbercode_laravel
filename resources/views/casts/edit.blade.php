@extends('layouts.master')

<!-- Content Header (Page header) -->
@section('header')
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <h1 class="text-center">{{$title}}</h1>
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">{{$title}}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
@endsection

<!-- Main content -->
@section('content')
<div class="row d-flex justify-content-center">
<div class="col-7">
    <div class="card card-light m-auto">
        <div class="card-header">
          <h3 class="card-title">Isilah dengan lengkap</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form class="form-horizontal" action="/cast/{{$cast->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="card-body pb-0">
              <div class="form-group row">
                <label for="name_el" class="col-sm-2 col-form-label">Nama</label>
                <div class="col-sm-10">
                  <input name="name" value="{{$cast->name}}" type="text" class="form-control @error('name') is-invalid @enderror" id="name_el" placeholder="Masukan nama lengkap artis" autocomplete="off">
                </div>
              </div>
              <div class="form-group row">
                <label for="age_el" class="col-sm-2 col-form-label">Umur</label>
                <div class="col-sm-3">
                  <input name="age" value="{{$cast->age}}" type="number" class="form-control @error('age') is-invalid @enderror" id="age_el" 
                  placeholder="tahun" autocomplete="off">
                </div>
              </div>
              <div class="form-group row">
                <label for="bio_el" class="col-sm-2 col-form-label">Bio</label>
                <div class="col-sm-10">
                  <textarea name="bio" type="text" class="form-control @error('bio') is-invalid @enderror" id="bio_el" rows="3" placeholder="Masukan biografi artis" autocomplete="off">{{$cast->bio}}</textarea>
                </div>
              </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer text-center pt-0">
              <a href="/cast/{{$cast->id}}" class="btn btn-default">Kembali</a>
              <button type="submit" class="btn btn-info">Edit</button>
            </div>
            <!-- /.card-footer -->
        </form>
    </div>

</div>
</div>
@endsection
    <!-- /.content -->
    