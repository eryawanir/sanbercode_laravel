@extends('layouts.master')

<!-- Content Header (Page header) -->
@section('header')
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <h1 class="text-center">{{$title}}</h1>
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">{{$title}}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
@endsection

<!-- Main content -->
@section('content')
<div class="card card-solid my-0 mx-4">
  <a href="/cast/create" class="col-3 btn btn-primary m-3">Tambah Artis</a>
  @if (session('success'))
    <div class="alert alert-success">
      {{ session('success')}}
    </div>
  @endif
    <div class="card-body pb-0">
      <div class="row">
          @forelse ($casts as $cast )   
          <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch flex-column">
            <div class="card bg-light d-flex flex-fill">
              <div class="card-header text-muted border-bottom-0">
                Actor
              </div>
              <div class="card-body pt-0">
                <div class="row">
                  <div class="col-7">
                    <h2 class="lead"><b>{{$cast->name}}</b></h2>
                    <p class="text-muted text-sm"><b>Bio: </b> {{$cast->bio}} </p>
                    <p class="text-muted text-sm"><b>Umur: </b> {{$cast->age}} tahun </p>
                  </div>
                  <div class="col-5 text-center">
                    <img src="{{asset('adminlte/dist/img/user1-128x128.jpg')}}" alt="user-avatar" class="img-circle img-fluid">
                  </div>
                </div>
              </div>
              <div class="card-footer">
                <div class="text-right">
                  <a href="cast/{{$cast->id}}" class="btn btn-sm btn-primary">
                    <i class="fas fa-user"></i> View Profile
                  </a>
                </div>
              </div>
            </div>
          </div>
          @empty
              <p>Data tidak ada</p>
          @endforelse
      </div>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
      <nav aria-label="Contacts Page Navigation">
        <ul class="pagination justify-content-center m-0">
          <li class="page-item active"><a class="page-link" href="#">1</a></li>
          <li class="page-item"><a class="page-link" href="#">2</a></li>
          <li class="page-item"><a class="page-link" href="#">3</a></li>
          <li class="page-item"><a class="page-link" href="#">4</a></li>
          <li class="page-item"><a class="page-link" href="#">5</a></li>
          <li class="page-item"><a class="page-link" href="#">6</a></li>
          <li class="page-item"><a class="page-link" href="#">7</a></li>
          <li class="page-item"><a class="page-link" href="#">8</a></li>
        </ul>
      </nav>
    </div>
    <!-- /.card-footer -->
</div>
@endsection
    <!-- /.content -->